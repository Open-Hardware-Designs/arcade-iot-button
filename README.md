# Arcade IoT Button

![](pictures/CHR_6627.jpg)

## What is it?

Enclosure for [30mm Arcade Button with LED](https://www.adafruit.com/product/3490). They are sold by adafruit or digikey. But I think you can find them in other places too.

## Why make it?

Initial idea was just a secret santa gift but then I made multiples of these for other people and at home. Since it has a mounting for ESP32 it can be made as an IoT button therefore the sky is the limit.

## Additional use cases

You can use this as just a regular button and connect button/LED wires directly to a microcontroller.
Can use ESP32 to have WiFi connectivity + your custom project
For basic functions [ESPHome](https://esphome.io/) is probably the easiest way to setup IoT button.
Or it can be used with [Home assistant](https://www.home-assistant.io/)


## Time tracking example

![](pictures/demo.gif)

I like to track time of various tasks or activities. Therefore, having this button on a table where I can just bash it to start the timer and then bash again to stop and send me a message with time delta in human readable time.

Basic data workflow.

Button press to ESP32.
ESPHome Publishes message to Mosquitto MQTT server.
In node-red I subscribe to the message and start counting the time.
Once pressed again node-red calculates the delta.
Prepares a text message and activates a python script which sends it to telegram chat via telegramBot.

Button - ESP32 - MQTT - Node-Red - Telegram

This guide should work for raspberrypi setup or any other debian based server.

### BOM

1x Enclosure
1x ESP32 Board
2x 2 position headers 0.1in spacing
4x ~6cm wires
4x [CONN SOCKET 22-30AWG CRIMP GOLD](https://www.digikey.ca/en/products/detail/harwin-inc/M20-1160042/3728123?s=N4IgTCBcDaILZgAwFoCMqBsjEBYIF0BfIA)
4x [Bumper feet](https://www.digikey.ca/en/products/detail/3m/SJ61A1/1768456)
Shrink tube

### Assembly

1. Before printing check what is your ESP32 size and adjust it in FreeCAD under ***Enclosure*** Body and ***ESP back holder*** feature.
In some designs I had like 3 varients.

![](pictures/PXL_20210909_194357458.jpg)
![](pictures/PXL_20210909_194407599.jpg)
![](pictures/Selection_615.png)
![](pictures/Selection_613.png)
![](pictures/Selection_614.png)

2. Print. No support needed.

![](pictures/3dprint-button.gif)

3. Cut wires
4. Strip wires

![](pictures/2_Strip-wires.gif)

5. Crimp wires

![](pictures/4_crimp-wires.gif)

6. Insert into housing

![](pictures/5_insert-into-housing.gif)

7. Insert shrink tube

![](pictures/6_shrink-tube.gif)

8. Solder Push Button and LED wires

![](pictures/7_solder-button.gif)

9. Heat shrink tubes

![](pictures/7b_heat-shrink-tubes.gif)

10. Insert Nut into Enclosure

![](pictures/8_insert-nut.gif)

11. Insert Button and make sure wires pass through the nut

![](pictures/9_insert-button.gif)

12. Turn the nut until tightened

![](pictures/10_screw-nut.gif)

13. Choose GPIO Pins. On ESP32 Dev kit I am ***using pins that have GND next to them***

|Name|Pin|
|---|---|
|LED | 21|
|Button |19|

14. Connect to ESP32 pins.

![](pictures/11_connect-esp.gif)

15. Insert ESP32

![](pictures/12_insert-esp.gif)

16. Add rubber feet

![](pictures/13_rubber-feet.gif)

### MQTT Broker

Install or know IP of your MQTT Broker.
I used [Mosquitto broker](https://mosquitto.org/download/)

### ESPHome firmware setup

Check out how to setup ESPHome.
And use this template.

1. Navigate to IoT_files directory
2. Setup python environment (tested on Python 3.7.7)
``` sh
python3 -m venv env
```
3. Activate environment
``` sh
source env/bin/activate
```
4. Upgrade pip
``` sh
pip install --upgrade pip
```
5. Install esphome
``` sh
pip install esphome
```
6. Create ***secrets.yaml*** file with content of your wifi credentials

``` Yaml
ssid_name: "xxxxxxxxx"
ssid_psw: "xxxxxxxxx"

ap_ssid_name: "xxxxx"
ap_ssid_psw: "xxxx"
```

7. Update IoT_files/arcade_button.yaml Board from [platformio list](https://platformio.org/boards?count=1000&filter%5Bplatform%5D=espressif32)
or try running esphome wizard and find your board in that list
8. Setup [MQTT server](https://mosquitto.org/download/) and update broker IP in ***arcade_button.yaml***
9. Update binary_sensor and output pins
10. Compile and flash firmware
``` sh
esphome arcade_button.yaml run
```

### TelegramBot python script

Create Bot using [BotFather](https://core.telegram.org/bots#6-botfather)
Once bot is created you will have bot API token.

On the same server as node-red setup python env or re-use esphome env

Install telegram bot libraries
```
pip install python-telegram-bot
```

Run command to find out your chat_id with your token
Add telegram bot to a channel type anything in the channel and bot will respond with it's ID

```
python3 telegram_chat_id.py --token abcd:123456798
```
Then close the script ```Ctrl+C```

![](pictures/chat-id.gif)

Get full path for python env and [telegram_notifications](IoT_files/telegram_notifications.py).  We will use them in next step

### Node-red Flow

Install node-red from [getting started guide](https://nodered.org/docs/getting-started/raspberrypi)

```
bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)
```

Import [node-red-flow.json](IoT_files/node-red-flow.json)

Make sure MQTT node is connected to your server

![](pictures/Selection_622.png)
![](pictures/Selection_623.png)
![](pictures/Selection_624.png)



Modify ***Setup variables*** nodes with your Chat ID, bot token and path to python env and script.

![](pictures/Selection_625.png)

### Test it out!
