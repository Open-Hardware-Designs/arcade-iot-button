#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Instantiate the parser
parser = argparse.ArgumentParser(description='Simple telegram Bot notification relay bla bla bla')

# Required positional argument
parser.add_argument('--token', type=str,
                    help='API Token')

parser.add_argument('--chat_id', type=int,
                    help='Chat ID')

parser.add_argument('--message', type=str,
                    help='Message to publish')

args = parser.parse_args()

print("Argument values:")
print(args.token)
print(args.chat_id)
print(args.message)

token = args.token
chat_id = args.chat_id
message = args.message

updater = Updater(token, use_context=True)
dp = updater.dispatcher
dp.bot.send_message(chat_id=chat_id,text=message)
